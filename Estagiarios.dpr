program Estagiarios;

uses
  Forms,
  uPrincipal in 'uPrincipal.pas' {fmPrincipal},
  uCadastrar in 'uCadastrar.pas' {fmCadastrar},
  uListar in 'uListar.pas' {fmListar},
  uPesquisar in 'uPesquisar.pas' {fmPesquisar},
  uSalario in 'uSalario.pas' {fmSalario};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmPrincipal, fmPrincipal);
  Application.CreateForm(TfmCadastrar, fmCadastrar);
  Application.CreateForm(TfmListar, fmListar);
  Application.CreateForm(TfmPesquisar, fmPesquisar);
  Application.CreateForm(TfmSalario, fmSalario);
  Application.Run;
end.
