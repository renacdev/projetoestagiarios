unit uCadastrar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uPrincipal, dxGDIPlusClasses;

type
  TfmCadastrar = class(TForm)
    Panel2: TPanel;
    edNome: TEdit;
    lb2: TLabel;
    edTelefone: TEdit;
    lb3: TLabel;
    lb4: TLabel;
    rd1: TRadioButton;
    rd2: TRadioButton;
    rd3: TRadioButton;
    edSalario: TEdit;
    lb5: TLabel;
    btnEnviar: TButton;
    Image1: TImage;
    Image2: TImage;
    Label1: TLabel;
    btnLimpar: TButton;
    procedure btnEnviarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure edTelefoneKeyPress(Sender: TObject; var Key: Char);
    procedure edSalarioKeyPress(Sender: TObject; var Key: Char);
    procedure edNomeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
//     novoCadastro = string;

  end;

var
  fmCadastrar: TfmCadastrar;
implementation

uses uListar;

{$R *.dfm}

function  Ret_Numero(Key: Char; Texto: string; EhDecimal: Boolean = False): Char;
begin
  if  not EhDecimal then
    begin
      if  not ( Key in ['0'..'9', Chr(8)] ) then
          Key := #0
    end
  else
    begin
      if  Key = #46 then
          Key := DecimalSeparator;
      if  not ( Key in ['0'..'9', Chr(8), DecimalSeparator] ) then
          Key := #0
      else if  ( Key = DecimalSeparator ) and ( Pos( Key, Texto ) > 0 ) then
          Key := #0;
    end;
     Result := Key;
end;

procedure TfmCadastrar.btnEnviarClick(Sender: TObject);
var
 a: integer;
begin

  if edNome.Text = '' then
    begin
      showMessage('Digite um nome');
          edNome.SetFocus;
          exit;
    end;
  if edTelefone.Text = '' then
    begin
      showMessage('Digite seu n�mero de telefone');
          edTelefone.SetFocus;
          exit;
    end;
  if edSalario.Text = '' then
    begin
      showMessage('Digite um sal�rio');
          edSalario.SetFocus;
          exit;
    end;
  if StrToInt(edSalario.Text) > 1000  then
    begin
      showMessage(' Valor MAIOR que o permitido'+#13+'Digite um valor entre R$ 400,00 e R$ 1000,00');
          edSalario.SetFocus;
          exit;
    end;
  if StrToInt(edSalario.Text) < 400  then
    begin
      showMessage(' Valor MENOR que o permitido'+#13+'Digite um valor entre R$ 400,00 e R$ 1000,00');
          edSalario.SetFocus;
          exit;
    end;


        for a := 7 downto 1 do
      begin
        cdNome[a]:= cdNome[a-1];
        cdTelefone[a]:= cdTelefone[a-1];
        cdSetor[a]:= cdSetor[a-1];
        cdSalario[a]:= cdSalario[a-1];
      end;

    cdNome[0]:= edNome.text;
    cdTelefone[0]:= edTelefone.text;
    if rd1.checked = true then
  cdSetor[0]:= rd1.caption
  else if rd2.checked = true then
  cdSetor[0]:= rd2.caption
  else if rd3.checked = true then
    begin
  cdSetor[0]:= rd3.caption;
    end;
    cdSalario[0]:= edSalario.text;


  if rd1.checked or rd2.checked or rd3.checked = false then
    begin
     showMessage('Informe o Setor');
     rd1.SetFocus;;
     exit;
    end;

  showMessage('Usu�rio Cadastrado');
  fmCadastrar.close;
end;

procedure TfmCadastrar.btnLimparClick(Sender: TObject);
begin
  edNome.Text:= '';
  edTelefone.Text:='';
  edSalario.Text:='';
  rd1.Checked := false;
  rd2.Checked := false;
  rd3.Checked := false;
end;

procedure TfmCadastrar.edNomeKeyPress(Sender: TObject; var Key: Char);
begin
     if Key in ['0'..'9'] then
     Key := #0;
end;

procedure TfmCadastrar.edSalarioKeyPress(Sender: TObject; var Key: Char);
begin
   Key := Ret_Numero( Key, edSalario.Text );

end;

procedure TfmCadastrar.edTelefoneKeyPress(Sender: TObject; var Key: Char);
begin
      Key := Ret_Numero( Key, edTelefone.Text );
end;
end.
