unit uListar;

interface

uses
  Windows, Messages, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfmListar = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    btnListar: TButton;
    procedure btnListarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
  end;

var
  fmListar: TfmListar;

implementation

uses uPrincipal, Sysutils;

{$R *.dfm}


procedure TfmListar.btnListarClick(Sender: TObject);
    var i: integer;
    a: integer;
    x: integer;
    margin: integer;
    marginT: integer;
    marginSe: integer;
    marginSa: integer;
    lbNome: array [0..15] of Tlabel;
    lbTelefone: array [0..15] of Tlabel;
    lbSetor: array [0..15] of Tlabel;
    lbSalario: array [0..15] of Tlabel;
//    est: array [0..15,4..7] of string;
  begin
    if btnListar.Tag = 1 then
    exit;
    btnListar.Tag:= 1;

       n:=6;
       margin:= 100;

       est[0,4]:= 'Daniel';
       est[1,4]:= 'Gustavo';
       est[2,4]:= 'Isac';
       est[3,4]:= 'Luca';
       est[4,4]:= 'Renan';
       est[5,4]:= 'Renato';
       est[0,5] := '85 98888-8888';
       est[1,5] := '85 98888-8888';
       est[2,5] := '85 98888-8888';
       est[3,5] := '85 98888-8888';
       est[4,5] := '85 98888-8888';
       est[5,5] := '85 98888-8888';
       est[0,6]:= 'Implantação';
       est[1,6]:= 'Desenvolvimento';
       est[2,6]:= 'Desenvolvimento';
       est[3,6]:= 'Suporte';
       est[4,6]:= 'Desenvolvimento';
       est[5,6]:= 'Desenvolvimento';
       est[0,7]:= IntToStr(400 + Random(600));
       est[1,7]:= IntToStr(400 + Random(600));
       est[2,7]:= IntToStr(400 + Random(600));
       est[3,7]:= IntToStr(400 + Random(600));
       est[4,7]:= IntToStr(400 + Random(600));
       est[5,7]:= IntToStr(400 + Random(600));

       for a := 6 to 15 do
        begin
          est[a,4]:= cdNome[a-6];
          est[a,5]:= cdTelefone[a-6];
          est[a,6]:= cdSetor[a-6];
          est[a,7]:= cdSalario[a-6];
        end;



  for i := 0 to 15  do
    begin
      lbNome[i] := Tlabel.create(application);
      lbNome[i].Parent := fmListar;
      lbNome[i].Caption:=est[i,4];
      lbNome[i].Top := margin + 20;
//      lbNome.font.size := 18;
      lbNome[i].Left := 100;
      margin:= margin + 20;

      lbTelefone[i] := Tlabel.create(application);
      lbTelefone[i].Parent := fmListar;
      lbTelefone[i].Caption:= est[i,5];
      lbTelefone[i].Top := margin;
      lbTelefone[i].Left := 225;
      marginT:= marginT + 20;

      lbSetor[i] := Tlabel.create(application);
      lbSetor[i].Parent := fmListar;
      lbSetor[i].Caption:= est[i,6];
      lbSetor[i].Top := margin;
      lbSetor[i].Left := 345;
      marginSe:= marginSe + 20;

      lbSalario[i] := Tlabel.create(application);
      lbSalario[i].Parent := fmListar;
      lbSalario[i].Caption:= est[i,7];
      lbSalario[i].Top := margin;
      lbSalario[i].Left := 470;
      marginSa:= marginSa + 20;

    end;
end;

procedure TfmListar.FormActivate(Sender: TObject);
begin
  btnListar.Tag:= 0;

end;

procedure TfmListar.FormClose(Sender: TObject; var Action: TCloseAction);
var
  x: integer;
begin
    btnListar.Tag:= 0;

  for x := 0 to 15 do
    begin
    end;
end;

end.
