unit uPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, dxGDIPlusClasses, ExtCtrls;

var
  n: integer;
  est: array [0..15,4..7] of string;
//  novoCadastro: array[0..4] of string;
  cdNome:array [0..15] of String;
  cdTelefone: array [0..15] of String;
  cdSetor: array [0..15] of String;
  cdSalario: array [0..15] of String;
  lbNome: TLabel;
  lbTelefone: TLabel;
  lbSetor: TLabel;
  lbSalario: TLabel;
  
type
  TfmPrincipal = class(TForm)
    btnCriar: TButton;
    btnListar: TButton;
    btnPesquisar: TButton;
    btnMedia: TButton;
    Image1: TImage;
    pnLaranja: TPanel;
    Image2: TImage;
    procedure btnCriarClick(Sender: TObject);
    procedure btnListarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnMediaClick(Sender: TObject);
  private
    { Private declarations }
  public
  end;


var
  fmPrincipal: TfmPrincipal;

implementation

uses  uCadastrar, uListar, uPesquisar, uSalario;

{$R *.dfm}

procedure TfmPrincipal.btnCriarClick(Sender: TObject);
begin
  fmCadastrar.show;
end;

procedure TfmPrincipal.btnListarClick(Sender: TObject);
begin
  lbNome := Tlabel.create(application);
  lbSalario := Tlabel.create(application);
  fmListar.show;
end;

procedure TfmPrincipal.btnMediaClick(Sender: TObject);
begin
  fmSalario.show;
end;

procedure TfmPrincipal.btnPesquisarClick(Sender: TObject);
begin
  fmPesquisar.show;
end;


end.
