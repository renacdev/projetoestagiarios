unit uSalario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uPrincipal;

type
  TfmSalario = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    btnMedia: TButton;
    btnListar: TButton;
    procedure btnMediaClick(Sender: TObject);
    procedure btnListarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSalario: TfmSalario;

implementation

{$R *.dfm}

procedure TfmSalario.btnListarClick(Sender: TObject);
var i: integer;
    x: integer;
    margin: integer;
    marginT: integer;
    marginSe: integer;
    marginSa: integer;
  begin
    if btnListar.Tag = 1 then
    exit;
    btnListar.Tag:= 1;

       n:=6;
       margin:= 120;

       est[0,4]:= 'Daniel';
       est[1,4]:= 'Gustavo';
       est[2,4]:= 'Isac';
       est[3,4]:= 'Luca';
       est[4,4]:= 'Renan';
       est[5,4]:= 'Renato';

       est[0,7]:= IntToStr(400 + Random(600));
       est[1,7]:= IntToStr(400 + Random(600));
       est[2,7]:= IntToStr(400 + Random(600));
       est[3,7]:= IntToStr(400 + Random(600));
       est[4,7]:= IntToStr(400 + Random(600));
       est[5,7]:= IntToStr(400 + Random(600));

  for i := 0 to n  do
    begin
      lbNome := Tlabel.create(application);
      lbNome.Parent := fmSalario;
      lbNome.Caption:=est[i,4];
      lbNome.Top := margin + 20;
      lbNome.Left := 100;
      margin:= margin + 20;

      lbSalario := Tlabel.create(application);
      lbSalario.Parent := fmSalario;
      lbSalario.Caption:= est[i,7];
      lbSalario.Top := margin;
      lbSalario.Left := 470;
      marginSa:= marginSa + 20;

    end;
end;

procedure TfmSalario.btnMediaClick(Sender: TObject);
var
  media: integer;
  soma: integer;
  s1: integer;
  s2: integer;
  s3: integer;
  s4: integer;
  s5: integer;
  s6: integer;
//  s7: integer;
begin
    Randomize;
      est[0,7]:= IntToStr(400 + Random(600));
      est[1,7]:= IntToStr(400 + Random(600));
      est[2,7]:= IntToStr(400 + Random(600));
      est[3,7]:= IntToStr(400 + Random(600));
      est[4,7]:= IntToStr(400 + Random(600));
      est[5,7]:= IntToStr(400 + Random(600));

      s1:= StrToInt(est[0,7]);
      s2:= StrToInt(est[1,7]);
      s3:= StrToInt(est[2,7]);
      s4:= StrToInt(est[3,7]);
      s5:= StrToInt(est[4,7]);
      s6:= StrToInt(est[5,7]);
//      s7:= StrToint(est[n,19]);

//      if IntToStr(s7) <> '' then
//        begin
//          soma:= s1+s2+s3+s4+s5+s6+s7;
//          media:= soma Div 7;
//        end
//      else
      soma:= s1+s2+s3+s4+s5+s6;
      media:= soma Div 6;

     showMessage('R$ '+IntToStr(media)+',00');
end;

end.
